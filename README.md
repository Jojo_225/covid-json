# Python

Ce projet s'inscrit dans le cadre du cours de Python du Master ES/EQUADE dispensé par M. ALEXIS BIENVENÜE à l'ISFA. Dans ce cadre, ce projet réaliser par NASSA A. JOSUE, KOUNOUDJI K. CRESUS et DIALLO MADANY a pour objectif général l' évaluation de la réaction de la population française dans la période covid.

Les données qui ont servi à notre projet sont issues de plusieurs sites : - www.data.gouv.fr -> sur ce site, nous avons téléchargé la base de données principale contenant pour chaque département de la France, pour chaque tranche d’âge, pour chaque jour (depuis le 27-12-2020 jusqu’aujourd'hui), des statistiques portant sur les vaccins... Plus précisément, vous trouverez des infos sur le contenu de la base via ce lien https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-personnes-vaccinees-contre-la-covid-19-1/.

Il y a aussi le site de l'insee sur lequel d'autres bases ont été téléchargé et qui ont été jointes à la base principale (https://insee.fr).

Enfin, la base de donnée d'extension .json qui va nous servir à délimiter les régions de la France métropolitaine pour notre illustration.

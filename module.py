# module.py
import pandas as pd
import matplotlib.pyplot as plt


def agrega(data: pd.core.frame.DataFrame,
           reference: list, stat: list):
    if ((set(reference)-set(data.columns.unique()) == set()) &
            (set(stat)-set(data.columns.unique()) == set())):
        result = data.groupby(reference).agg(
            {col: "sum" for col in stat}).reset_index()
        return result
    else:
        print("""l'une des listes des variables en paramètre
        n'est pas incluse dans la base de données renseignées""")


def evolution(data: pd.core.frame.DataFrame,
              axe_y: list,
              label: list,
              title: list = [""],
              start: str = "2021-01-01",
              end: str = "2022-01-01",
              unique_plot: bool = False):
    if set(axe_y)-set(data.columns.unique()) == set():
        if (unique_plot is True) & (len(axe_y) != len(label)):
            print("""Désolé, mais vu que vous avez choisi
            de représenter tous les graphs sur un seul plot,
            la longueur de la liste de axe_y, doit être égale
            à celle de la liste des label.""")
        elif (unique_plot is False) & (len(axe_y) != len(title)):
            print("""Désolé, mais vu que vous avez choisi
            de représenter tous les graphs sur des plots différents,
                la longueur de la liste de axe_y, doit être égale à
                celle de la liste des titres de graphique.
                Si vous ne souhaitez pas de titre, vous pouvez
                par exemple entrer ["",""]
                dans le cas où vous voulez tracer 2 courbes.
                """)
        elif unique_plot is True:
            plt.figure(figsize=(18, 5))
            for i in axe_y:
                plt.plot(data.jour[((data.jour >= start) &
                                   (data.jour <= end))],
                         data[i][((data.jour >= start) &
                                 (data.jour <= end))],
                         label=label[axe_y.index(i)])
            plt.legend()
            plt.title(title[0])
            plt.show()
        elif unique_plot is False:
            fig, ax = plt.subplots(len(axe_y), 1)
            for i in range(len(axe_y)):
                ax[i].plot(data.jour[((data.jour >= start) &
                                     (data.jour <= end))],
                           data[axe_y[i]][((data.jour >= start)
                                          & (data.jour <= end))],
                           label=title[i])
            plt.legend()
            plt.tight_layout()
            plt.show()
    else:
        print("""l'une des listes des variables en paramètre n'est pas
        incluse dans la base de données renseignées""")

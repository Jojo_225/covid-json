# pip install xlrd
# pip install datetime

# Bibliothèques
import module
import numpy as np
import copy
import pandas as pd
import folium
import matplotlib.pyplot as plt
import requests
import zipfile
import io
import tempfile
import json
import datetime

# importation
url = "https://www.data.gouv.fr/fr/datasets/" \
      "r/64605b19-b3bd-4c26-9f30-e0c3915290d3"
response = requests.get(url)
file = pd.read_csv(io.StringIO(response.content.decode(
    'utf-8')), sep=";", decimal=",")  # 250 Mo
file.head()

# Voir les noms des variables
file.columns
# VARIABLES RETENUES
file = file.loc[:, ['dep', 'clage_vacsi', 'jour',
                    'n_dose1_h', 'n_dose1_f', 'n_dose1_e',
                    'n_cum_dose1_h', 'n_cum_dose1_f', 'n_cum_dose1_e']]
file

# Indentification de chaque colonne
print("""clage_vacsi=classes d’âge
dep=departement
n_dose1_h=Nombre de personnes ayant reçu une 1ere injection
de vaccin au niveau des hommes
n_dose1_f=Nombre de personnes ayant reçu une 1ere injection
de vaccin au niveau des femmes
n_dose1_e=Nombre de personnes ayant reçu une 1ere injection
de vaccin
n_cum_dose1_h=Nombre cumulé de personnes ayant reçu
une 1ere injection de vaccin au niveau des hommes
n_cum_dose1_h=Nombre cumulé de personnes ayant reçu
une 1ere injection de vaccin au niveau des femmes
n_cum_dose1_h=Nombre cumulé de personnes ayant reçu
une 1ere injection de vaccin""")


# Vérifier si le jeu de données contient des valeurs manquantes, aberrantes
# Convertir le format de la variable "jour" en DATE

file.jour = file.jour.apply(lambda x: datetime.datetime.fromisoformat(x))
file = file.sort_values(
    by=['jour', 'dep', 'clage_vacsi'], ascending=[True, True, True])
file

# vérification des valeurs manquantes
print(file.apply(lambda x: x.count()).sort_values(
    ascending=False))  # pas de valeurs manquantes

# vérification de valeurs aberrantes

print("""Nous allons nous assurer que le type des données est uniforme
par variable pour #toutes les lignes et aussi conforme à nos attentes.
Par exemple, au niveau du #département, on s'attend à avoir que
des chiffres d'après les informations du #site. Une manière simple
de vérifier si nous avons des chiffres est d'utiliser
la méthode sum() puis de diviser par un chiffre""")

for i in [0, 1, 3, 4, 5, 6, 7, 8]:
    try:
        file.iloc[:, i].to_numpy().sum()/5
    except (TypeError, ValueError):
        print(file.columns[i],
              "contient des lignes qui " +
              "ne contiennent pas des chiffres")

    # Nous allons chercher à comprendre la source de cette incohérence.
pd.Series([type(file.iloc[x, 0])
          for x in range(len(file["dep"]))]).value_counts()

# Ci-dessus, nous venons de voir qu'il y a 1270889 lignes qui sont
# des entiers, et le reste des chaines de caractères.
file['dep'].unique()

# Certaines valeurs de la colonne "dep" contiennent des lettres.
# Du coup, dans le souci d'attribuer un format identique
# à tous les éléments de la colonne "dep", nous allons choisir
# de convertir tous les éléments de dep en chaine de caractère

file.dep = file.dep.astype(str)

# Ce code-ci révèle que les cellules ont été parfaitement
# transformées en string
pd.Series([type(file.iloc[x, 0])
          for x in range(len(file["dep"]))]).value_counts()
file['dep'].unique()

# Agregation des valeurs

print("""Transformation du tableau pour avoir des valeurs agregées:
-par jour
-par département et par jour
-par classe d'âge et par jour""")

# module personnalisé
file_date = module.agrega(file,
                          reference=["jour"],
                          stat=["n_dose1_h",
                                "n_dose1_f",
                                "n_dose1_e",
                                "n_cum_dose1_h",
                                "n_cum_dose1_f",
                                "n_cum_dose1_e"])
file_dep = module.agrega(file,
                         reference=["dep", "jour"],
                         stat=["n_dose1_h",
                               "n_dose1_f",
                               "n_dose1_e",
                               "n_cum_dose1_h",
                               "n_cum_dose1_f",
                               "n_cum_dose1_e"])

# aperçue dep
file_dep
# aperçue date
file_date


module.evolution(file_date,
                 axe_y=["n_dose1_e", "n_dose1_h", "n_dose1_f"],
                 title=["Evolution de nombre de personnes " +
                        "vaccinées une première fois"
                        ],
                 label=["Ensemble", "Homme", "Femme"],
                 unique_plot=True)

print("""On peut noter 4 vagues de vaccin:
-une première entre le début de l'année 2021 et Mars 2021
-une 2ème entre Mars 2021 et Mai 2021
-une 3ème entre Mai 2021 et Juillet 2021
-une 4ème entre Juillet 2021 et Octobre 2021
Les évolutions du nombre de personnes vaccinées
une première fois sont identiques quel que soit le genre.""")

module.evolution(file_date,
                 axe_y=["n_cum_dose1_e", "n_cum_dose1_h",
                        "n_cum_dose1_f"],
                 title=["Evolution cumulée de nombre de personnes " +
                        "vaccinées une première fois"
                        ],
                 label=["Ensemble", "Homme", "Femme"],
                 unique_plot=True)

print("""Les évolutions du nombre cumulé de personnes vaccinées
une première fois sont identiques quel que soit le genre.""")


# TELECHARGER LES BASES POUR LA JOINTURE


# URL du fichier ZIP à télécharger
zip_url = "https://www.insee.fr/fr/statistiques/fichier/4316069/" \
    "departement2020-csv.zip"
# Téléchargement du fichier ZIP
response = requests.get(zip_url)
# Extraction du fichier ZIP en mémoire
zip_file = zipfile.ZipFile(io.BytesIO(response.content))
# Création d'un répertoire temporaire pour extraire le fichier CSV
temp_dir = tempfile.TemporaryDirectory()
# Extraction du fichier CSV dans le répertoire temporaire
zip_file.extractall(temp_dir.name)
# Chemin complet du fichier CSV extrait
csv_file_path = temp_dir.name + "/departement2020.csv"
# Chargement du fichier CSV dans un DataFrame
dep2020 = pd.read_csv(csv_file_path, sep=",", encoding='utf-8')
# Supprimer le répertoire temporaire
temp_dir.cleanup()
# Afficher le DataFrame
dep2020


# URL du fichier ZIP à télécharger
zip_url = "https://www.insee.fr/fr/statistiques/fichier/4316069/" \
    "region2020-csv.zip"
# Téléchargement du fichier ZIP
response = requests.get(zip_url)
# Extraction du fichier ZIP en mémoire
zip_file = zipfile.ZipFile(io.BytesIO(response.content))
# Création d'un répertoire temporaire pour extraire le fichier CSV
temp_dir = tempfile.TemporaryDirectory()
# Extraction du fichier CSV dans le répertoire temporaire
zip_file.extractall(temp_dir.name)
# Chemin complet du fichier CSV extrait
csv_file_path = temp_dir.name + "/region2020.csv"
# Chargement du fichier CSV dans un DataFrame
reg2020 = pd.read_csv(csv_file_path, sep=",", encoding='utf-8')
# Supprimer le répertoire temporaire
temp_dir.cleanup()
# Afficher le DataFrame
reg2020


# Selection de colonnes
dep2020 = dep2020.iloc[:, [0, 1, 6]]
dep2020


# Selection et renommer
reg2020 = reg2020.iloc[:, [0, 5]]
reg2020 = reg2020.rename(columns={"libelle": "nom_reg"})
reg2020


# Etape consistant à conserver dans une variable "carte" toutes
# les observations
# du 1er jour de chaque mois dans toute la base jusqu'au 01-03-2022

file_dep['jour'] = file_dep['jour'].astype(str)
carte = file_dep[file_dep['jour'].str.endswith('01')]
carte = carte[carte.jour < "2022-03-01"]
carte.reset_index(drop=True, inplace=True)

carte

# Faire une jointure entre "carte" et "reg2020" en
# transitant par dep2020
# En effet, on aimerait que la variable carte possède une colonne
# indiquant le nom des régions de chaque observation.
# On ne peut joindre la variable "carte" et "reg2020" directement.
# Nous allons les joindre passant par "dep2020",
# car cette variable possède une variable commune à carte (dep)
# et une autre commune à reg (reg)

carte = pd.merge(carte, dep2020, on="dep", how="left")
carte

carte = pd.merge(carte, reg2020, on="reg", how="left")
carte

# grouper et agreger le nombre de vaccin par region
carte_group = module.agrega(carte,
                            reference=['reg', "nom_reg", "jour"],
                            stat=["n_dose1_h",
                                  "n_dose1_f",
                                  "n_dose1_e",
                                  "n_cum_dose1_h",
                                  "n_cum_dose1_f",
                                  "n_cum_dose1_e"])
carte_group


# Faire disparaitre les caractères spéciaux
carte_group = carte_group.applymap(
    lambda x: x.replace("ô", "o") if isinstance(x, str) else x)
carte_group = carte_group.applymap(
    lambda x: x.replace("é", "e") if isinstance(x, str) else x)
carte_group = carte_group.applymap(
    lambda x: x.replace("Î", "I") if isinstance(x, str) else x)
carte_group.nom_reg.unique()


# Télécharger une base contenant l'effectif des régions par région.
# Ces effectifs serviront à comparer les statistiques
# pour 1000 habitants.
url = "https://www.insee.fr/fr/statistiques/fichier/1893198/estim-" \
    "pop-nreg-sexe-gca-1975-2023.xls"
response = requests.get(url)
if response.status_code == 200:
    pop = pd.read_excel(response.content, sheet_name="2021",
                        skiprows=4, nrows=13, usecols="A:G")
    pop = pop.iloc[:, [0, 6]]
    # pour supprimer les carctères spéciaux
    pop = pop.applymap(lambda x: x.replace("ô", "o")
                       if isinstance(x, str) else x)
    pop = pop.applymap(lambda x: x.replace("Î", "I")
                       if isinstance(x, str) else x)
    pop = pop.applymap(lambda x: x.replace("é", "e")
                       if isinstance(x, str) else x)
    pop = pop.applymap(lambda x: x.replace(
        "Centre-Val-de-Loire",
        "Centre-Val de Loire") if isinstance(x, str) else x)
    new_columns = ["nom_reg", "population"]
    pop.rename(columns=dict(zip(pop.columns, new_columns)),
               inplace=True)
    print(pop)
else:
    print("Erreur lors du téléchargement du fichier")


# FUSIONNER LES BASES
carte_group = pd.merge(carte_group, pop, on="nom_reg", how="left")
carte_group["vac_1000"] = 1000 * \
    carte_group["n_dose1_e"]/carte_group["population"]
carte_group["vac_1000_cum"] = 1000 * \
    carte_group["n_cum_dose1_e"]/carte_group["population"]


# TELECHARGEMENT DU FICHIER .JSON QUI SERVIRA A FAIRE
# NOS ILLUSTRATIONS SUR LA CARTE DE LA FRANCE

# URL du fichier ZIP à télécharger
zip_url = "https://geodata.ucdavis.edu/gadm/gadm4.1/json/" \
    "gadm41_FRA_1.json.zip"
# Téléchargement du fichier ZIP
response = requests.get(zip_url)
# Extraction du fichier ZIP en mémoire
zip_file = zipfile.ZipFile(io.BytesIO(response.content))
# Création d'un répertoire temporaire pour extraire le fichier CSV
temp_dir = tempfile.TemporaryDirectory()
# Extraction du fichier json dans le répertoire temporaire
zip_file.extractall(temp_dir.name)
# Chemin complet du fichier json extrait
json_file_path = temp_dir.name + "/gadm41_FRA_1.json"
# Chargement du fichier json en format txt
with open(json_file_path, "r", encoding="utf-8") as file:
    json_data = file.read()
# Supprimer le répertoire temporaire
temp_dir.cleanup()


# Suppression des caractères spéciaux
# Correction de certains noms
# Dans le but d'avoir des modalités uniformes
# au niveau du nom des région.
json_data = json_data.replace("Î", "I")
json_data = json_data.replace("é", "e")
json_data = json_data.replace("ô", "o")
json_data = json_data.replace("GrandEst", "Grand Est")
json_data = json_data.replace("PaysdelaLoire", "Pays de la Loire")
json_data = json_data.replace("Centre-ValdeLoire",
                              "Centre-Val de Loire")
json_data = json_data.replace("Provence-Alpes-Coted'Azur",
                              "Provence-Alpes-Cote d'Azur")
json_data = json_data.replace("ô", "o")
json_data = json_data.replace("ô", "o")

json_data = json.loads(json_data)


# ILLUSTRATION GRAPHIQUE

def carte(col=['nom_reg', 'n_cum_dose1_e'], date="2021-01-01"):
    france_map = folium.Map(location=[46.603354, 1.888334], zoom_start=6)
    folium.Choropleth(
        geo_data=json_data,
        name='statistique',
        data=carte_group[carte_group.jour == date],
        columns=col,
        key_on='feature.properties.NAME_1',
        fill_color='YlOrRd',
        fill_opacity=0.7,
        line_opacity=0.2,
        legend_name='Statistique',
        bins=4
    ).add_to(france_map)
    folium.LayerControl().add_to(france_map)
    # france_map.save("carte_france.html")
    return france_map


carte(date="2021-01-01")
carte(date="2021-02-01")
carte(date="2021-03-01")
carte(date="2021-04-01")
carte(date="2021-05-01")
carte(date="2021-06-01")
carte(date="2021-07-01")
carte(date="2021-08-01")
carte(date="2021-09-01")
carte(date="2021-10-01")
carte(date="2021-11-01")
carte(date="2021-12-01")


def carte(col=['nom_reg', 'vac_1000_cum'], date="2021-01-01"):
    france_map = folium.Map(location=[46.603354, 1.888334], zoom_start=6)
    folium.Choropleth(
        geo_data=json_data,
        name='statistique',
        data=carte_group[carte_group.jour == date],
        columns=col,
        key_on='feature.properties.NAME_1',
        fill_color='YlOrRd',
        fill_opacity=0.7,
        line_opacity=0.2,
        legend_name='Statistique',
        bins=4
    ).add_to(france_map)
    folium.LayerControl().add_to(france_map)
    # france_map.save("carte_france.html")
    return france_map


carte(date="2021-01-01")
carte(date="2021-02-01")
carte(date="2021-03-01")
carte(date="2021-04-01")
carte(date="2021-05-01")
carte(date="2021-06-01")
carte(date="2021-07-01")
carte(date="2021-08-01")
carte(date="2021-09-01")
carte(date="2021-10-01")
carte(date="2021-11-01")
carte(date="2021-12-01")

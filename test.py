# pip instal requests

import unittest
import pandas as pd
import matplotlib.pyplot as plt
import requests
from module import agrega


def test_agrega():
    data = pd.DataFrame({'A': [1, 1, 2, 2],
                         'B': [1, 2, 3, 4],
                         'C': [5, 6, 7, 8]})
    reference = ['A']
    stat = ['B', 'C']
    expected_result = pd.DataFrame({'A': [1, 2],
                                    'B': [3, 7],
                                    'C': [11, 15]})
    result = agrega(data, reference, stat)
    assert result.equals(expected_result)


test_agrega()


def test_download_data():
    url = "https://www.data.gouv.fr/fr/datasets/" \
          "r/64605b19-b3bd-4c26-9f30-e0c3915290d3"
    response = requests.get(url)
    if response.status_code == 200:
        print("Téléchargement de la grande base de données réussi")
    else:
        print("Échec du téléchargement de la grande base de données")


test_download_data()


def test_download_data():
    url = "https://www.insee.fr/fr/statistiques/fichier/4316069/"\
          "departement2020-csv.zip"
    response = requests.get(url)
    if response.status_code == 200:
        print("Téléchargement de la 1ère base pour jointure réussi")
    else:
        print("""Échec du téléchargement de la 1ère base
        pour jointure  des données""")


test_download_data()


def test_download_data():
    url = "https://www.insee.fr/fr/statistiques/fichier/4316069/"\
          "region2020-csv.zip"
    response = requests.get(url)
    if response.status_code == 200:
        print("Téléchargement de la 2ème base pour jointure réussi")
    else:
        print("""Échec du téléchargement de la 2ème base
        pour jointure  des données""")


test_download_data()


def test_download_data():
    url = "https://www.insee.fr/fr/statistiques/fichier/1893198/estim-" \
          "pop-nreg-sexe-gca-1975-2023.xls"
    response = requests.get(url)
    if response.status_code == 200:
        print("Téléchargement de la 3ème base pour jointure réussi")
    else:
        print("""Échec du téléchargement de la 3ème base
        pour jointure  des données""")


test_download_data()


def test_download_data():
    url = "https://geodata.ucdavis.edu/gadm/gadm4.1/json/" \
          "gadm41_FRA_1.json.zip"
    response = requests.get(url)
    if response.status_code == 200:
        print("Téléchargement du fichier json réussi")
    else:
        print("""Échec du téléchargement du fihier json""")


test_download_data()
